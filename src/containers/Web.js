import React, { Component }  from 'react'
import Website from '../images/website.svg'
import MobileApp from '../images/mobile-app.svg'
import Marketing from '../images/marketing.svg'
// import App from '../images/app.png'
// import Reclam from '../images/reclam.png'
import { Link } from 'react-router-dom';

export default class Web extends Component {
    render() {
        return (
            <div className="home_bg">
                <div className="home_bg-wrapper animated">
                    <Link to={`/web/site`} className="home_bg-wrapper-site hover animated fadeInRight delay_3">
                        <img src={Website} alt="photos" className="img" />
                        <h2 className="h2">Веб сайты</h2>
                        <div className="wrap">
                            <div className="p">Универсальный дизайн</div>
                            <div className="p">Адаптация под любые разрешение</div>
                            <div className="p">Доступная цена</div>
                        </div>
                        <button className="inner-btn">Подробнее</button>
                    </Link>
                    <Link to={`/web/application`} className="home_bg-wrapper-site hover animated fadeInRight delay_4">
                        <img src={MobileApp} alt="photos" className="img" />
                        <h2 className="h2">Веб приложение</h2>
                        <div className="wrap">
                            <div className="p">Функциональный прототип</div>
                            <div className="p">Правильная техническая структура</div>
                            <div className="p">Надежное функционирование</div>
                        </div>
                        <button className="inner-btn">Подробнее</button>
                    </Link>
                    <Link to={`/web/marketing`} className="home_bg-wrapper-site hover animated fadeInRight delay_5">
                        <img src={Marketing} alt="photos" className="img" />
                        <h2 className="h2">Маркетинг</h2>
                        <div className="wrap">
                            <div className="p">Широкий охват аудитории</div>
                            <div className="p">Таргетинг контингента</div>
                            <div className="p">Продвижение в соцсетях</div>
                        </div>
                        <button className="inner-btn">Подробнее</button>
                    </Link>
                </div>
            </div>            
        )
    }
}
