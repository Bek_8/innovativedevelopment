import React, { Component } from 'react'
import AboutPhoto1 from '../images/about1.svg'
import AboutPhoto2 from '../images/about2.svg'
import AboutPhoto3 from '../images/about3.svg'
import AboutPhoto4 from '../images/about4.svg'
import AboutPhoto5 from '../images/about5.svg'
import AboutPhoto6 from '../images/about6.svg'
import AboutPhoto7 from '../images/about7.svg'
import AboutPhoto8 from '../images/about8.svg'
import { Link } from 'react-router-dom';

export default class About extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="home_bg">
                    <div className="about">
                        <div className="container">
                        <h2 className="main-title animated fadeIn">Наши направления</h2>
                        <div className="about__list">
                            <Link to={`/web/site`} className="about__list-block animated fadeInRight delay_1">
                                <div className="image">
                                    <img src={AboutPhoto1} alt="about" />
                                </div>
                                <div className="title">Веб сайты</div>
                                <div className="information">
                                    Реализация сайтов различных типов. От лэндинга до самых сложных и нестандартных 
                                </div>
                            </Link>
                            <Link to={`/web/application`} className="about__list-block animated fadeInRight delay_2">
                                <div className="image">
                                    <img src={AboutPhoto2} alt="about" />
                                </div>
                                <div className="title">Веб приложения</div>
                                <div className="information">
                                    Полноценное программное обеспечение для онлайн оптимизации бизнеса
                                </div>
                            </Link>
                            <Link to={`/mobile`} className="about__list-block animated fadeInRight delay_3">
                                <div className="image">
                                    <img src={AboutPhoto3} alt="about" />
                                </div>
                                <div className="title">Мобильные приложения</div>
                                <div className="information"> 
                                    Разработка для iOS и Android. Лучшая производительность, удобство в использовании
                                </div>
                            </Link>
                            <Link to={`/web/site`} className="about__list-block animated fadeInRight delay_4">
                                <div className="image">
                                    <img src={AboutPhoto4} alt="about" />
                                </div>
                                <div className="title">Интернет-магазин</div>
                                <div className="information">Создаем платформы исключив участие человека. Покупки одним кликом, экономия ресурсов и времени.</div>
                            </Link>
                            <Link to={`/web/site`} className="about__list-block animated fadeInRight delay_2">
                                <div className="image">
                                    <img src={AboutPhoto5} alt="about" />
                                </div>
                                <div className="title">Дизайн</div>
                                <div className="information">Займемся воплошением и айдентикой вашей идеи в цифровую реальность, для увеличение продаж</div>
                            </Link>
                            <Link to={`/web/site`} className="about__list-block animated fadeInRight delay_3">
                                <div className="image">
                                    <img src={AboutPhoto6} alt="about" />
                                </div>
                                <div className="title">Копирайтинг</div>
                                <div className="information">Напишем текст Вашего контента который будет говорить за Вас своей креативностью и содержанием</div>
                            </Link>
                            <Link to={`/web/marketing`} className="about__list-block animated fadeInRight delay_4">
                                <div className="image">
                                    <img src={AboutPhoto7} alt="about" />
                                </div>
                                <div className="title">SEO</div>
                                <div className="information">Проведем комплекс мер, для продвижения и узнаваемости Вашего сайта в поисковых системах</div>
                            </Link>
                            <Link to={`/web/marketing`} className="about__list-block animated fadeInRight delay_5">
                                <div className="image">
                                    <img src={AboutPhoto8} alt="about" />
                                </div>
                                <div className="title">SMM</div>
                                <div className="information">Будем продвигать Вас в социальных сетях путем Digital-аналитики и таргетинга</div>
                            </Link>
                        </div>
                    </div>
                    </div>
                </div>
            </React.Fragment>
             
        )
    }
}
