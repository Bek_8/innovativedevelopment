import React, { Component } from 'react'
import { Link , NavLink } from 'react-router-dom';
import Logo from '../images/logo.svg';

export default class Nav extends Component {
    constructor(props){
        super(props);
        this.state = {
            isOpen:false
        }
    }
    toggleHamb = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    closeHamburger = () => {
        this.setState({
            isOpen: false
        });
    }
    linkComponent = (name , path) => {
        return ( 
            <div className="home_left-menu-div">
                <NavLink exact={true} to={`${path ? path : '/'}`} activeClassName="active" onClick={this.closeHamburger}>{name}</NavLink>
            </div>
        );
    }
    render() {
        return (
            <React.Fragment>
            <div className={`modal ${this.state.isOpen ? 'open' : ''}`} onClick={this.closeHamburger}></div>
            <header className="home_left">
                <div className="home_left-info">
                    <div className="home_left-info-logo">
                        <Link to="/" className="animated fadeInRight fast">
                            <img src={Logo} className="logo" alt="Logo" />
                        </Link>
                    </div>
                </div>
                <nav className={`home_left-menu ${this.state.isOpen ? 'opened' : ''}`}>
                    {this.linkComponent('ГЛАВНОЕ','/')}
                    {this.linkComponent('УСЛУГИ','/about')}
                    {this.linkComponent('ВЕБ РАЗРАБОТКА','/web')}
                    {this.linkComponent('MOBILE DEV','/mobile')}
                    {this.linkComponent('ПОРТФОЛИО','/portfolio')}
                    {this.linkComponent('КОНТАКТЫ','/contacts')}
                </nav>
                <div className="hamburger sm-show">
                    <svg className={`ham hamRotate ham4 ${this.state.isOpen ? 'active' : ''}`} viewBox="0 0 100 100" width="80" onClick={this.toggleHamb}>
                    <path className="line top"
                            d="m 70,33 h -40 c 0,0 -8.5,-0.149796 -8.5,8.5 0,8.649796 8.5,8.5 8.5,8.5 h 20 v -20" />
                    <path className="line middle"
                            d="m 70,50 h -40" />
                    <path className="line bottom"
                            d="m 30,67 h 40 c 0,0 8.5,0.149796 8.5,-8.5 0,-8.649796 -8.5,-8.5 -8.5,-8.5 h -20 v 20" />
                    </svg>
                </div>
            </header>
            </React.Fragment>  
        )
    }
}
