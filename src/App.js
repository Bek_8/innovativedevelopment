import React , {Component} from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import About from './containers/About';
import Contact from './containers/Contact';
import Home from './containers/Home';
import Mobile from './containers/Mobile';
import Web from './containers/Web';
import Design from './containers/Design';
import Portfolio from './containers/Portfolio';
import Site from './containers/Site';
import Marketing from './containers/Marketing';
import WebApp from './containers/WebApp';
import NotMatch from './containers/NotMatch';
import Nav from './components/Nav';
import './scss/styles.scss';

export default class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      modalIsOpen: false,
      name: "",
      phone: "",
      successModal: false
    };
  }
  switchModal = () =>{
    this.setState({
      modalIsOpen: !this.state.modalIsOpen
    });
  }
  closeModal = () =>{
    this.setState({
      modalIsOpen: false,
      successModal: false
    });
  }  
  sendPost = async (event)  => {
    event.preventDefault(); 
    const url = 'https://demo.indev.uz/api/checkout';
    const data = { name: this.state.name , phone: this.state.phone };
    if(this.state.phone.length > 1 && this.state.name.length > 1)
    try {
      const response = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(data), 
        headers: {
          'Content-Type': 'application/json'
        }
      });
      const json = await response.json();
      let result = JSON.stringify(json);
      if(result === 'ok'){
        this.setState({ successModal: true, modalIsOpen:false ,name:'',phone:''});
        setTimeout(() => {
          this.setState({successModal: false})
        },5000);
      }
    } catch (error) {
    }    
  }
  handleChangeName = (event) => {
    this.setState({name: event.target.value});
  }
  handleChangePhone = (event) => {
    this.setState({phone: event.target.value});
  }  
  render(){
    return (
      <Router>
        <div className="wrapper">
          <button className="order" onClick={this.switchModal}>Заказать</button>          
          <div className={`modal ${this.state.modalIsOpen || this.state.successModal ? "open" : ''}`} onClick={this.closeModal}></div>
          <div className={`form ${this.state.successModal ? "open" : ''}`}>
            <div className="top">
              <div className="left">
                  <p className="bold">Заявка принята успешно!</p>
                  <p className="secondary">Скоро мы свяжемся с Вами!</p>
              </div>
              <div className="right">
                  <svg 
                  viewBox="0 0 41 52" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M41 20.8326C41 9.32024 31.8286 0 20.5 0C19.3714 0 18.2571 0.087105 17.1714 0.275833V15.7079C17.1714 18.5534 14.9 20.8471 12.1143 20.8471H6.65714V20.8326C6.65714 17.0436 8.12857 13.6174 10.5143 11.0914V2.62767C4.24286 6.19898 0 13.0077 0 20.8326V20.8471V27.6123H13.4571C19.1857 27.6123 23.8286 22.8941 23.8286 17.0726V7.17165C29.8714 8.68147 34.3429 14.2272 34.3429 20.8326V20.8471H30.4857C30.4857 24.5781 33.4714 27.6123 37.1429 27.6123H41V20.8471V20.8326Z" fill="url(#paint0_linear)"/>
  <path d="M36.1429 32.5483C36.1429 36.2793 33.1572 39.3134 29.4857 39.3134H4.85718C4.85718 35.5824 7.84289 32.5483 11.5143 32.5483H36.1429Z" fill="#F9E480"/>
  <path d="M34.1999 32.5483C34.1999 36.2793 31.2142 39.3134 27.5428 39.3134H6.79993C6.79993 35.5824 9.78564 32.5483 13.4571 32.5483H34.1999Z" fill="url(#paint1_linear)"/>
  <path d="M31.2857 44.2494C31.2857 47.9804 28.3 51.0145 24.6285 51.0145H9.71423C9.71423 47.2835 12.6999 44.2494 16.3714 44.2494H31.2857Z" fill="#F9E480"/>
  <path d="M29.3001 44.2494C29.3001 47.9804 26.3144 51.0145 22.6429 51.0145H11.7001C11.7001 47.2835 14.6858 44.2494 18.3572 44.2494H29.3001Z" fill="url(#paint2_linear)"/>
  <path d="M32.5857 6.69257C39.2285 12.7028 40.7285 20.6729 40.9999 25.2314V20.8471V20.8326C40.9999 9.32024 31.8285 0 20.4999 0C19.3857 0 18.2857 0.087105 17.2285 0.261315C19.6857 0.435525 26.5714 1.24851 32.5857 6.69257Z" fill="#F9E480"/>
  <defs>
  <linearGradient id="paint0_linear" x1="6.45867" y1="34.1784" x2="34.9933" y2="6.09931" gradientUnits="userSpaceOnUse">
  <stop stopColor="#F9E480"/>
  <stop offset="1" stopColor="#E0CA46"/>
  </linearGradient>
  <linearGradient id="paint1_linear" x1="6.80261" y1="35.9293" x2="34.1972" y2="35.9293" gradientUnits="userSpaceOnUse">
  <stop stopColor="#F9E480"/>
  <stop offset="1" stopColor="#E0CA46"/>
  </linearGradient>
  <linearGradient id="paint2_linear" x1="11.6933" y1="47.6254" x2="29.3068" y2="47.6254" gradientUnits="userSpaceOnUse">
  <stop stopColor="#F9E480"/>
  <stop offset="1" stopColor="#E0CA46"/>
  </linearGradient>
  </defs>
  </svg>
              </div>
            </div>
            <div className="center text-center">
              <svg id="successAnimation" className="animated" xmlns="http://www.w3.org/2000/svg" width="200" viewBox="0 0 70 70">
                <path id="successAnimationResult" fill="#09d3ac" d="M35,60 C21.1928813,60 10,48.8071187 10,35 C10,21.1928813 21.1928813,10 35,10 C48.8071187,10 60,21.1928813 60,35 C60,48.8071187 48.8071187,60 35,60 Z M23.6332378,33.2260427 L22.3667622,34.7739573 L34.1433655,44.40936 L47.776114,27.6305926 L46.223886,26.3694074 L33.8566345,41.59064 L23.6332378,33.2260427 Z"/>
                <circle id="successAnimationCircle" cx="35" cy="35" r="24" stroke="#09d3ac" strokeWidth="2" strokeLinecap="round" fill="transparent"/>
                <polyline id="successAnimationCheck" stroke="#09d3ac" strokeWidth="2" points="23 34 34 43 47 27" fill="transparent"/>
              </svg>              
            </div>
          </div>
            <div className={`form ${this.state.modalIsOpen ? "open" : ''}`}>
              <div className="top">
                <div className="left">
                  <p className="bold">Оставьте заявку</p>
                  <p className="secondary">И мы свяжемся с вами</p>
                </div>
                <div className="right">
                <svg width="41" height="52"
                 viewBox="0 0 41 52" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M41 20.8326C41 9.32024 31.8286 0 20.5 0C19.3714 0 18.2571 0.087105 17.1714 0.275833V15.7079C17.1714 18.5534 14.9 20.8471 12.1143 20.8471H6.65714V20.8326C6.65714 17.0436 8.12857 13.6174 10.5143 11.0914V2.62767C4.24286 6.19898 0 13.0077 0 20.8326V20.8471V27.6123H13.4571C19.1857 27.6123 23.8286 22.8941 23.8286 17.0726V7.17165C29.8714 8.68147 34.3429 14.2272 34.3429 20.8326V20.8471H30.4857C30.4857 24.5781 33.4714 27.6123 37.1429 27.6123H41V20.8471V20.8326Z" fill="url(#paint0_linear)"/>
<path d="M36.1429 32.5483C36.1429 36.2793 33.1572 39.3134 29.4857 39.3134H4.85718C4.85718 35.5824 7.84289 32.5483 11.5143 32.5483H36.1429Z" fill="#F9E480"/>
<path d="M34.1999 32.5483C34.1999 36.2793 31.2142 39.3134 27.5428 39.3134H6.79993C6.79993 35.5824 9.78564 32.5483 13.4571 32.5483H34.1999Z" fill="url(#form-point1_linear)"/>
<path d="M31.2857 44.2494C31.2857 47.9804 28.3 51.0145 24.6285 51.0145H9.71423C9.71423 47.2835 12.6999 44.2494 16.3714 44.2494H31.2857Z" fill="#F9E480"/>
<path d="M29.3001 44.2494C29.3001 47.9804 26.3144 51.0145 22.6429 51.0145H11.7001C11.7001 47.2835 14.6858 44.2494 18.3572 44.2494H29.3001Z" fill="url(#form-point2_linear)"/>
<path d="M32.5857 6.69257C39.2285 12.7028 40.7285 20.6729 40.9999 25.2314V20.8471V20.8326C40.9999 9.32024 31.8285 0 20.4999 0C19.3857 0 18.2857 0.087105 17.2285 0.261315C19.6857 0.435525 26.5714 1.24851 32.5857 6.69257Z" fill="#F9E480"/>
<defs>
<linearGradient id="form-point0_linear" x1="6.45867" y1="34.1784" x2="34.9933" y2="6.09931" gradientUnits="userSpaceOnUse">
<stop stopColor="#F9E480"/>
<stop offset="1" stopColor="#E0CA46"/>
</linearGradient>
<linearGradient id="form-point1_linear" x1="6.80261" y1="35.9293" x2="34.1972" y2="35.9293" gradientUnits="userSpaceOnUse">
<stop stopColor="#F9E480"/>
<stop offset="1" stopColor="#E0CA46"/>
</linearGradient>
<linearGradient id="form-point2_linear" x1="11.6933" y1="47.6254" x2="29.3068" y2="47.6254" gradientUnits="userSpaceOnUse">
<stop stopColor="#F9E480"/>
<stop offset="1" stopColor="#E0CA46"/>
</linearGradient>
</defs>
</svg>
                </div>
              </div>
              <form className="center" onSubmit={this.sendPost}>
                <div className="input-form">
                  <label htmlFor="name">Имя</label>
                  <input type="text" id="name" value={this.state.name} onChange={(event) => this.handleChangeName(event)} required={true} name="name" />
                </div>
                <div className="input-form">
                  <label htmlFor="phone">Номер телефона</label>
                  <input type="phone" id="phone" required={true} value={this.state.phone} onChange={(event) => this.handleChangePhone(event)} name="phone" />
                </div>
                <div className="text-center">
                  <button className="form-button">Отправить</button>
                </div>
              </form>
            </div>
          <div className="home">
            <Nav />
            <Switch>
            <Route path="/" exact render={(props)=><Home switchModal={this.switchModal}  />} />
            <Route path="/web" exact component={Web} />
            <Route path="/web/site" exact component={Site} />
            <Route path="/web/application" exact component={WebApp} />
            <Route path="/web/marketing" exact component={Marketing} />
            <Route path="/contacts" exact component={Contact} />
            <Route path="/about" exact component={About} />
            <Route path="/mobile" exact component={Mobile} />
            <Route path="/design" exact component={Design} />
            <Route path="/design/first" exact component={Design} />
            <Route path="/design/second" exact component={Design} />
            <Route path="/design/third" exact component={Design} />
            <Route path="/portfolio" exact component={Portfolio} />
            <Route component={NotMatch} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}